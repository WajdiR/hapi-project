import Hapi from "@hapi/hapi";
import { notesRoutes } from "../src/routes/notes.routes";

let server: Hapi.Server;

beforeAll(async () => {
  server = Hapi.server({
    port: 3000,
    host: "localhost",
  });
  notesRoutes(server);
  await server.initialize();
});

afterAll(async () => {
  await server.stop();
});

describe("POST /notes", () => {
  it("should create a new note", async () => {
    const response = await server.inject({
      method: "POST",
      url: "/notes",
      payload: {
        title: "Test Note",
        body: "This is a test note",
      },
    });

    expect(response.statusCode).toBe(201);
    expect(JSON.parse(response.payload)).toHaveProperty("id");
  });
});

describe("GET /notes", () => {
  it("should retrieve all notes", async () => {
    const response = await server.inject({
      method: "GET",
      url: "/notes",
    });

    expect(response.statusCode).toBe(200);
    expect(Array.isArray(JSON.parse(response.payload))).toBe(true);
  });
});
describe("GET /notes/{id}", () => {
  it("should retrieve a note by id", async () => {
    // Assuming 'some-valid-id' is a valid ID in your test database
    const response = await server.inject({
      method: "GET",
      url: "/notes/some-valid-id",
    });

    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.payload)).toHaveProperty("id", "some-valid-id");
  });
});
describe("PUT /notes/{id}", () => {
  it("should update a note", async () => {
    // Assuming 'some-valid-id' is a valid ID in your test database
    const response = await server.inject({
      method: "PUT",
      url: "/notes/some-valid-id",
      payload: {
        title: "Updated Note",
        body: "This is an updated note",
      },
    });

    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.payload)).toHaveProperty(
      "title",
      "Updated Note"
    );
  });
});
describe("DELETE /notes/{id}", () => {
  it("should delete a note", async () => {
    // Assuming 'some-valid-id' is a valid ID in your test database
    const response = await server.inject({
      method: "DELETE",
      url: "/notes/some-valid-id",
    });

    expect(response.statusCode).toBe(204);
  });
});
